import Ember from 'ember';
import sweetAlert2 from '../index';

export default Ember.Service.extend(sweetAlert2);
