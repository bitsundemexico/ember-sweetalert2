/* eslint-env node */
'use strict';

module.exports = {
  name: 'ember-sweetalert2',
  options: {
    nodeAssets: {
      'sweetalert2': {
        import: {
          srcDir: 'dist',
          include: ['sweetalert2.js', 'sweetalert2.css']
        },
      }
    }
  },
  included: function( parent ) {
    this.addonOptions = parent.options;
    this._super.included.apply(this, arguments);
  }
};
